export PATH_SEP    := \
export EXEC_SUFFIX := .exe
export RANLIB      := ranlib
export MV          := mv
export AR          := ar
export RM          := rm -f
export LN          := ln -sf
export CP          := cp
export CPP         := cpp -E -C -P
export CC          := gcc

export PLATFORMS   := win32
export DEFAULT_PLATFORM := win32

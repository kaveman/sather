------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 199x by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

abstract class $RO_DIGRAPH{NTP} < $GRAPH{NTP,DIEDGE{NTP}} is
   -- NTP is the type of the node i.e. the type of the unique node
   -- index.  This is a read-only digraph abstraction. No modifying
   -- operations are permitted. Most views of digraphs are of this
   -- type
   
   
   -- Inherits:
   -- size: INT; str: STR; has(n: NTP): BOOL; elt!: NTP;
   -- n_nodes:INT,n_edges:INT,n_adjacent(n: NTP): INT
   -- node!: NTP, edge!:DIEDGE{NTP}, adjacent!(n:NTP):NTP
   -- has_node(n: NTP):BOOL, has_edge(n:NTP):BOOL
   
   copy: $RO_DIGRAPH{NTP};
   -- Create a copy of this digraph. The copy is also read-only
   
   n_incoming(n: NTP): INT;
   -- Return the number of incoming edges into the node "n"
   
   n_outgoing(n: NTP): INT;
   -- Return the number of outgoing edges from the node "n"

   --              ------ Cursor --------------------------
   incoming!(once n: NTP): NTP;
   -- Yield the incoming edges into the node "n". No ordering
   -- is guaranteed
   
   outgoing!(once n: NTP): NTP;
   -- Yield the outgoing edges from the node "n"
   
   -- Equality testing
   equals(g: $RO_DIGRAPH{NTP}): BOOL;
   -- Return true if the self and "g" have the same structure and the
   -- same nodes. The nodes must be the *same* (i.e. =), and in
   -- both graphs the must be the same edges between nodes
   
end;
-------------------------------------------------------------------   
abstract class $DIGRAPH{NTP} < $RO_DIGRAPH{NTP}  is
   -- A directed graph that permits the addition of new nodes and edges.

   add_node: NTP;
   -- Add a new node to the graph. Returns the node index

   add_node(n: NTP): NTP;
   -- Add the node "n" to the graph. Return a node index.  The node
   -- index that is returned may be the same as the original node
   -- index, if that is appropriate for this graph, or maybe a new
   -- object.  The user of this function should make no assumption
   -- about the relationship between the node that is added and the
   -- index that is returned.
   
   connect(e: DIEDGE{NTP});	
   -- Add the edge "e" which connects the nodes "e.first" to
   -- "e.second"

   delete_node(n: NTP);
   -- Delete the node index "n"

   disconnect(e: DIEDGE{NTP});	
   -- Delete the edge specified by "e". Does nothing if
   -- "e" does no exist

end;
------------------------------------------------------------------- 

   

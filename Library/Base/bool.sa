------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1994 by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- bool.sa: Boolean values.
-------------------------------------------------------------------
immutable class BOOL < $STR, $IS_EQ, $FMT is
   -- BOOL objects represent boolean values and are either equal to 
   -- `true' or `false'. The boolean operators `and' and `or' are 
   -- part of the Sather language. This class defines several 
   -- additional operators.

   
   not:SAME is
      -- The complement of self.
      -- if self then return false else return true end 
      builtin BOOL_NOT; 
   end;
   
   xnor(b:SAME):SAME is
      -- True if self and `b' have the same value (same as "is_eq").
      builtin BOOL_IS_EQ; 
   end;
   
   is_eq(b:SAME):SAME is
      -- True if self and `b' have the same value (same as "xnor").
      builtin BOOL_IS_EQ; 
   end;
   
   is_eq(arg: $OB): BOOL is
      -- Overloaded version of the is_eq routine that works with an argument
      -- of any type. If the type of the 'arg' is not the same as they
      -- type of 'self' then return false. Otherwise, deletegate to
      -- the 'real' is_eq(SAME):BOOL routine
      typecase arg  when SAME then return is_eq(arg)  else return false end;
   end;
   
   xor(b:SAME):SAME is
      -- Self exclusive ored with `b'.  Same as "/=".
      return self/=b end;
   
   nand(b:SAME):SAME is
      -- The complement of self anded with `b'.
      return ~(self and b) end;
   
   nor(b:SAME):SAME is
      -- The complement of self ored with `b'.
      return ~(self or b) end;
   
   implies(b:SAME):SAME is
      -- True iff self implies `b'. Same as "nand_not".
      return not or b 
   end;
   
   and_rout(b:SAME):SAME is
      -- A routine version of "self and `b'". (Useful for making
      -- bound routines.)
      return self and b 
   end;
   
   or_rout(b:SAME):SAME is
      -- A routine version of "self or `b'". (Useful for making
      -- bound routines.)
      return self or b 
   end;   

   and_not(b:SAME):SAME is
      -- Computes self and the complement of `b'.
      return self and ~b 
   end;

   or_not(b:SAME):SAME is
      -- Computes self or the complement of `b'.
      return self or ~b 
   end;
   
   nand_not(b:SAME):SAME is
      -- Computes self nand the complement of `b'. This is the same as
      -- the complement of self or `b'.
      return not or b 
   end;

   nor_not(b:SAME):SAME is
      -- Computes self nor the complement of `b'. This is the same as
      -- the complement of self and `b'.
      return not and b 
   end;
   
   int:INT is
      -- 0 for false, 1 for true
      builtin BOOL_INT; 
   end;
   
   str:STR is
      -- The string representation of self.
      if self then return "true" else return "false" end 
   end;

   fmt( f: STR ):STR is
      return BASE_FORMAT::fmt_bool( self, f )
   end;
   
   from_str(s:STR):SAME is
      case s
      when "true","t","True","T","TRUE" then return true;
      when "false","f","False","F","FALSE" then return false;
      else raise "Can't interpret bool value: "+s;
      end;
   end;

end; -- immutable class BOOL

-------------------------------------------------------------------

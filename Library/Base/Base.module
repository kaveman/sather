--------------------------->  GNU Sather - module  <---------------------------
-- Copyright (C) 1994 by International Computer Science Institute            --
-- This file is part of the GNU Sather package. It is free software; you may --
-- redistribute  and/or modify it under the terms of the  GNU General Public --
-- License (GPL)  as  published  by the  Free  Software  Foundation;  either --
-- version 3 of the license, or (at your option) any later version.          --
-- This  program  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/GPL for more details.        --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

(* BASE CLASSES

The base classes are the primitives out of which all Sather
classes may be built.  Most are given special treatment by
the compiler.

Some examples of literals for built-in Sather types:

    ARRAY - | a, b, c |
    BOOL  - true
    CHAR  - 'a'
    FLT   - 1.0
    FLTD  - 1.0d
    INT   - 56
    STR   - "abc"

AREF provides an array portion to classes which include it.
If you want more functionality such as sorting, the ARRAY class
(in Containers) is what you want.  AVAL is the equivalent of
AREF for value classes.

EXT_OB is used to carry values foreign to Sather, such as C pointers.

The 1.0 language spec defines FLT, FLTD, FLTX and FLTDX which
correspond the IEEE single, double, extended and double extended
respectively.  The current compiler only implements FLT and FLTD
however.  FLTI is an infinite precision floating point class; it
isn't implemented either.  However, take a look at RAT in Math.

Both signed and unsigned operations are provided on Sather INT;
there are not special types for these.

A complete description of the built-in Sather types can be found
in the language specification, which is in the Doc directory of
the Sather installation.

*)





-- This is a list of library files that can automatically
-- be loaded by a reference in users' SATHER_COMMANDS env variable

    abstract.sa -has abstract.sa 
	$IS_EQ $IS_LT $HASH $NIL $COPY $STR $ELT ELT_ALG
    compare.sa -has compare.sa COMPARE ELT_EQ ELT_LT ELT_NIL ELT_HASH COMPARABLE
    aref.sa -has aref.sa AREF 
    aval.sa -has aval.sa AVAL
    bool.sa -has bool.sa BOOL 
    char.sa -has char.sa CHAR 
    flt.sa -has flt.sa FLT C_FLT
    fltd.sa -has fltd.sa FLTD C_FLTD 
	
    succ_stream.sa -has succ_stream.sa $SUCC_STREAM INT_STREAM 

    int.sa -has int.sa INT 
    misc.sa -has misc.sa $OB EXT_OB CAST CAST_EXC
    elt_alg.sa -has elt_alg.sa ELT_ALG

    number.sa -has number.sa $NFE $NUMBER $REAL_NUMBER $CPX_NUMBER

    char_test.sa -has char_test.sa CHAR_TEST
    bool_test.sa -has bool_test.sa BOOL_TEST
    fltd_test.sa -has fltd_test.sa FLTD_OUTPUT_TEST
    int_test.sa -has int_test.sa INT_TEST
    aref_test.sa -has aref_test.sa AREF_TEST
    math_test.sa -has math_test.sa MATH_TEST FLT_TEST FLTD_TEST
 
	-- Dummy versions of non-existant FLT classes
    flt_dummy.sa -has flt_dummy.sa  FLTX FLTDX FLTI
	
   -- Non existant junk (for now). Unimplememented part of spec.
   --    flt_other.sa -has flt_other.sa FLT_UNDEF FLTX FLTDX FLTI

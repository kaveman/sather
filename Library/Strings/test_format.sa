------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1995 by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- test_format.sa: Formatting a string a la printf
-- Author: Benedict A. Gomes <gomes@icsi.berkeley.edu>
-- Author: Holger Klawitter <holger@icsi.berkeley.edu>
-- For documentation see:
--    http://www.icsi.berkeley.edu/~holger/Sather/format.html

class TEST_FMT
is
    include TEST;

    main is
	class_name("FMT");
	
	protect

	    test( "int",
		 #FMT("*%+3d+%-3d=%04d%%", 3, 4, 7),
		 "* +3+4  =0007%" );
	 
	    test( "escaping",
		 #FMT( "%% %< <> %> %d", 3, 4 ), "% < 3 > 4" );
	    
	    test( "flt and str",
		 #FMT("%10s is less than %8.3f", "(%)", 3.14159),
		 "       (%) is less than    3.142" );
	 
	    test( "char and fltd",
		 #FMT("%5c foo %g", 'x', 1.234d),
		 "    x foo 1.234" );
	 
	    test( "arg mixing",
		 #FMT("%2$d %1$d", 3, 4 ),
		 "4 3" );
	 
	    test( "arg mixing args implicit order",
		 #FMT("%2$d %d %1$d %d", 1, 2, 3),
		 "2 3 1 2" );
	 
	    FMT_ERROR::raise_exceptions := true;
	    
	    raise_test( "wrong types error",
		       bind(FMT::create("%s",7)),
		       "INT needs \"diouxX\" but got 's'." );
	 
	    raise_test( "end of format error I",
		       bind(FMT::create("%==")),
		       "Unexpected end of format in \"%==\"." );
	 
	    raise_test( "end of format error II",
		       bind(FMT::create("+%")),
		       "Unexpected end of format in \"+%\"." );

	    raise_test( "bad argument number I",
		       bind(FMT::create("%1$d %3$d",1,2)),
		       "Illegal argument number '3'." );
	 
	    raise_test( "bad argument number II",
		       bind(FMT::create("%2$d %d %d",1,2,3)),
		       "Illegal argument number '4'." );

	    test( "INT",
		 #FMT( "<###>+<^##>=<#^#>", 1, 2, 3 ),
		 "  1+2  = 3 " );
	 
	    test( "FLT precision exhausing",
		 #FMT( "<###.##>=<####>=<1:+>*<3:#.##########>",
		      3.1551, 3.22222, 4.0/3.0 ),
		 "  3.16=   3=+3.1551*1.3333334000" );

	    test( "rounding fixed point",
		 #FMT( "<#.#>$<#.##>$<#.###>$<#.####>$<#.##>",
		      0.9995, 0.9995, 0.9996, 0.9995, 0.096 ),
		 "1.0$1.00$1.000$0.9995$0.10" );
	  
	    test( "INT precision",
		 #FMT( "<###.##>$<.##>$<+###.>$<-###.>$<-.##>",
		      7, 8, 9, -10, -11 ),
		 "  7   $8   $  +9 $ -10 $-11   " );
	    
	    raise_test( "using anchor and precision in INT",
		       bind(FMT::create("<^##.##>",7)),
		       "Decimal char and `^' cannot be used together."
		       );
	 
	    test( "FLTD",
		 #FMT( "<-#.#######################>",
		      3.1234567890123456789012d ),
		 " 3.12345678901234570000000" );
	 
	    test( "anchors",
		 #FMT( "<###^##>*<###^##>*<###^##>*<###^##>*<###^##>*<##^##>",
		      12,123,1234,12345,123456,12 ),
		 "  12  *  123 * 1234 * 12345*123456* 12  " );
	    
	    test( "bases",
		 #FMT( "<hex####>*<bin####>*<oct####>", 140, 12, 130 ),
		 "  8C*1100* 202" );
	    
--	    test( "CPX",
--		 #FMT( "<##.#> @<polar##.#;#.###> @<##.###;>",
--		      #CPX(1.0,1.0),#CPX(0.0,1.0),#CPX(11.0,2.222) ),
--		 " 1.0+ 1.0i @ 1.0*e^1.571i @11.000+2.222i" );
	    
	    test( "string",
		 #FMT( "<>$<^##>$<F.###>$<F*##^##>", "xx", "yy", "yy", "zzz" ),
		 "xx$yy $.yy$*zzz*" );
	 
	    test( "bool and char",
		 #FMT( "<#####^#>$<F&#^##>", false, '%' ),
		 "  false$&%&&" );
	    
	    test( "decimal dots",
		 #FMT( "<#> <.> <,> <#.> <#,>", 1.0, 2.0, 3.0, 4.0, 5.0 ),
		 "1 2.0 3,0 4. 5," );

	    FMT_ERROR::raise_exceptions := false; -- this is the default !
	    
	    test( "returning string exceptions",
		 #FMT( "<> <^.> <>", 7,8,9 ),
		 "7 Decimal char and `^' cannot be used together. 9");
	    
	when $STR then
	    #ERR + "[" + exception.str + "]\n";
	    test("Happens never","true","false");
	else
	    raise exception
	end;
	finish;
    end; -- main
   
end; -- TEST_FMT

------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1995 by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- perm_random_gen.sa:
-- Author: Benedict A. Gomes <gomes@samosa.ICSI.Berkeley.EDU>
-- (Translated from S.Omohundro's code)

class PERM_RANDOM_GEN{G} < $RANDOM_GEN is
   -- A generator which randomly permutes the outputs of a generator of type
   -- `G' (which should be a subtype of `RANDOM_GEN') to break up serial
   -- correlations.
   
   private attr gen:G;		-- The generator whose output we permute.
   private attr tbl:ARRAY{FLTD};-- The permutation table.
   const tbl_size: INT := 97;	-- Nice round number?! Must be some
   -- reason - must ask Steve.

   create:SAME is
      -- A new permutation generator.
      res:=new; 
      res.tbl:=#ARRAY{FLTD}(tbl_size); 
      res.g:=make_gen; 
      init(1);
   end; -- create
   
   private make_gen: G is
      g ::= #G;
      typecase g
      when G then return g
      else raise("Generator does not return SAME") end;
   end;

   init(nseed:INT) is
      -- Initialize both the generator `g' and the table using `nseed'.
      g.init(nseed);		-- initialize the generator
      i:INT; loop until!(i=tbl_size);	-- initialize the table
	 tbl[i]:=g.get;
	 i:=i+1
      end; -- loop
   end; -- init
      
   get:FLTD is
      -- Pseudo-random value in `[0.,1.)' constructed from `g' by
      -- permutation.
      nv:FLTD:=g.get;		-- new random value
      ti:INT:=(nv*tbl_size).int;	-- index to swap with
      res:=tbl[ti]; tbl[ti]:=nv;
   end; -- get
   
end; -- class PERM_RANDOM_GEN{G}
-------------------------------------------------------------------------

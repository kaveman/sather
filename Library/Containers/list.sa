------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1995 by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- list.sa: Extensible array
-- Author: Benedict A. Gomes <gomes@samosa.ICSI.Berkeley.EDU>

abstract class $LIST{ETP} < $ARR{ETP} is
   -- An extensible array abstraction. Similar to a map abstraction,
   -- but keys are integers; after an insert, all other keys may have
   -- changed

   is_empty:BOOL;
   -- Returns true if the size of the container = 0

   size: INT;
   -- Number of elements contained
   
   copy: SAME;
    -- Return a copy of the current container

   has(e: ETP): BOOL;
   -- True if the container contains the element "e"
   
   elt!:ETP;
   -- Yield all the elements of self. The order is not defined.
   
   str:STR;
   -- Yield a string version of self

   equals(a: $RO_ARR{ETP}): BOOL;
   -- Return true if self and 'a' contain the same elements in the
   -- same order

   insert_after(ind: INT, val: ETP):$LIST{ETP};
   --    pre valid_ind(ind) post valid_ind(result); 
   -- Insert the value "val" after the index "ind" in the list The
   -- indices of all subseqeuent elements from initial[ind+1] to the
   -- end will be shifted up by 1
   
   insert_before(ind: INT,val: ETP):$LIST{ETP};
   --   Insert the value "val" before the index "ind" in the list
   -- The indices of all subsequent elements, from initial[ind] onward
   -- will be shifted upward by 1

   insert_all_before(ind: INT, val: $CONTAINER{ETP}):$LIST{ETP};
   --   Insert the elements of "val" in order, before "ind". The indices
   -- of all indices from initial[ind] upward to the end will be
   -- shifted upward
   
   insert_all_after(ind: INT, val: $CONTAINER{ETP}):$LIST{ETP};
   -- Insert the elements of the list in order after the element
   -- initial[ind]. The indices of all subsequent elements from
   -- initial[ind+1]will be shifted upward
   
   append(e: ETP):$LIST{ETP};
   -- Concatenate "e" onto the end of a copy of self

   append_all(l: $CONTAINER{ETP}):$LIST{ETP};
   -- post has(l.elt!) 
   -- Append the elements of "l" onto the end of a copy of self

   insert_after(ind: INT, val: ETP);
   -- pre valid_ind(ind) post valid_ind(result);
   -- Insert the value "val" after the index "ind" in the list The
   -- indices of all subseqeuent elements from initial[ind+1] to the
   -- end will be shifted up by 1
   
   insert_before(ind: INT,val: ETP);
   -- The indices of all subsequent elements, from initial[ind] onward
   -- will be shifted upward by 1

   insert_all_before(ind: INT, val: $CONTAINER{ETP});
   --   Insert the elements of "val" in order, before "ind". The indices
   -- of all indices from initial[ind] upward to the end will be
   -- shifted upward
   
   insert_all_after(ind: INT, val: $CONTAINER{ETP});
   -- Insert the elements of the list in order after the element
   -- initial[ind]. The indices of all subsequent elements from
   -- initial[ind+1]will be shifted upward
   
   append(e: ETP);
   -- Concatenate "e" onto the end of the array

   append_all(l: $CONTAINER{ETP});
   -- post has(l.elt!) 
   -- Append the elements of "l" onto the end of the current array

end; -- class $LIST{ETP}
-------------------------------------------------------------------



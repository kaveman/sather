------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1995 by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- Author: Holger Klawitter <holger@icsi.berkeley.edu>

immutable class TK_VAR
is
    include TK_TAG_INCL;
    
    query: STR
    is
	return GUI_APP_END::eval("queryVariable "+str);
    end;
    
end; -- class TKVAR
-------------------------------------------------------------------
class TK_CHECK_CFG < $TK_WIDGET_CFG
is
    include TK_BUTTON_CFG;
    
    variable(v:TK_VAR) 
    -- Variable associated to this widget. Changes to the variable
    -- cause the widget to update and user actions on this widget
    -- may change the value of the variable.
    is
	config("variable",v.str);
    end;
    
    on_value(s:STR)
    -- Value for the associated variable when the checkbox is selected.
    is
	config("onvalue",s); 
    end;

    off_value(s:STR)
    -- Value for the associated variable when the checkbox is not selected.
    is
	config("offvalue",s);
    end;

    -- For conveniant usage.
    variable(v:TK_VAR): SAME is variable(v); return self; end;
    on_value(s:STR): SAME is on_value(s); return self; end;
    off_value(s:STR): SAME is off_value(s); return self; end;
    
    std: SAME is return new.relief_flat end;
    
end; -- class TK_CHECK_CFG
----------------------------------------------------------------------
class TK_CHECK < $TK_WIDGET
-- Widget representing checkboxes (checkbuttons in TkTcl).
is
    include TK_WIDGET_INCL{TK_CHECK_CFG};
    
    const tk_widget_type: STR := "checkbutton";
    private default_init(c:TK_CHECK_CFG) is
	eval(widget_name+" configure -command ",
	     quote("sather "+path_name+" 0"));
    end;
    private default_config: TK_CHECK_CFG is return TK_CHECK_CFG::std end;
    
    invoke is eval(actual_widget_name,"invoke") end;
    flash is eval(actual_widget_name,"flash") end;
    
    -- Routines to change the state of the widget.
    select is eval(actual_widget_name,"select") end;
    deselect is eval(actual_widget_name,"deselect") end;
    toggle is eval(actual_widget_name,"toggle") end;

    readonly attr command: ROUT;
    -- Associated command which will be invoked when the checkbox
    -- is selected.
    command(c:ROUT): SAME is command(c); return self; end;
    
    act_on(id:INT,args:ARRAY{STR})
    is
	if ~void(command) then command.call; end
    end;
    
end; -- class TK_CHECK

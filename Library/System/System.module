--------------------------->  GNU Sather - module  <---------------------------
-- Copyright (C) 1994 by International Computer Science Institute            --
-- This file is part of the GNU Sather package. It is free software; you may --
-- redistribute  and/or modify it under the terms of the  GNU General Public --
-- License (GPL)  as  published  by the  Free  Software  Foundation;  either --
-- version 3 of the license, or (at your option) any later version.          --
-- This  program  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/GPL for more details.        --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

(*

SYSTEM CLASSES

The system library is separated out since some portions of this library
are not available on all systems. Check the 
SATHER_HOME/Systme/Platforms/<your system>/Platform.module 
to see what is installed on your platform. <your system> refers to
the name of the platform on which you have installed Sather.
Most things will work on most unix systems, but may require, for 
instance, tcl/tk to be installed.

An extremely useful routine in sys.sa is SYS::ob_eq, which
performs an object-identity equality test.  (In other words,
is it exactly the same object, as opposed to another object which
has the same fields.)  Many of the container classes use this notion
of equality if the contained class does not provide an equality
test of its own.

The TEST class makes it convenient to build test classes for other
classes with uniform reporting.  Please use it to build your test
code.


*)

-- This is a list of library files that can automatically
-- be loaded by a reference in users' SATHER_COMMANDS env variable

    runtime.sa -has runtime.sa RUNTIME
    sys.sa -has sys.sa SYS
    test.sa -has test.sa TEST TEST_TEST
    
    time.sa -has time.sa TIME TIMES MEMORY_USAGE
    unix.sa -has unix.sa UNIX C_UNIX
    Zones/Zones.module

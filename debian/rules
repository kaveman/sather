#!/usr/bin/make -f
############################################################################
# Made with the aid of dh_make, by Craig Small
# Based on sample debian/rules that uses debhelper,  by Joey Hess.
# Some lines taken from debmake, by Cristoph Lameter.
# Based on a earlier version by Shugo Maeda
# Completely reworked and turned upside down by Norbert Nemec
############################################################################

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

pwd := $(shell pwd)

###################

sather_root  := $(pwd)/debian/tmp
sather_home  := $(sather_root)/usr/lib/sather
sather_bin   := $(sather_root)/usr/bin

sather_files := Bin/sacomp \
		Library \
		System/FORBID \
		System/Debug \
		System/Common/*.c \
		System/Common/*.h \
		System/Common/*.module \
		System/Common/CONFIG \
		System/Platforms/X \
		System/Platforms/dualgui \
		System/Platforms/f77 \
		System/Platforms/gui \
		System/Platforms/linux \
		System/Platforms/tcltk

###################

lwp_root    := $(pwd)/debian/sather-lwp
lwp_home    := $(lwp_root)/usr/lib/sather

###################

doc_root    := $(pwd)/debian/sather-doc
doc_dir     := $(doc_root)/usr/doc/sather-doc
doc_info    := $(doc_root)/usr/info

info_files  := sather.info sather.info-1 sather.info-2 sather-tutorial.info

###################

el_root    := $(pwd)/debian/sather-elisp
el_install := $(el_root)/usr/lib/emacsen-common/packages/install
el_remove  := $(el_root)/usr/lib/emacsen-common/packages/remove
el_dir     := $(el_root)/usr/share/emacs/site-lisp/sather-elisp
el_etc     := $(el_root)/etc/emacs/site-start.d
el_info    := $(el_root)/usr/info

el_files   := Emacs/sather.el Emacs/sather-module.el Emacs/sather-lib.el

###################

browser_root := $(pwd)/debian/sather-browser
browser_home := $(browser_root)/usr/lib/sather

browser_files := Bin/sabrowse Bin/gen_html Browser/Tcl Browser/Web

############################################################################

all:

###################

build: build-force build-stamp
build-force:
	rm -f build-stamp
build-stamp:
#	dh_testversion
	dh_testdir
	dh_testroot
	
	$(MAKE) DEFAULT_PLATFORM="linux" PLATFORMS="linux,linux/lwp" compiler
	$(MAKE) DEFAULT_PLATFORM="linux" PLATFORMS="linux,linux/lwp" browser
	$(MAKE) DEFAULT_PLATFORM="linux" PLATFORMS="linux,linux/lwp" info
	
	touch build-stamp

###################

install: install-force install-stamp
install-force:
	rm -f install-stamp
install-stamp: build-stamp
#	dh_testversion
	dh_testdir
	dh_testroot
	
	##### General preparations for installing
	
	dh_clean -k
	dh_installdirs
	
	##### Installing: sather
	
	for file in $(sather_files); do \
	  cp -a -P $$file $(sather_home); \
	done
	
	rm -r `find $(sather_root) "(" -name "*.config" -o -name "CVS" -o -name "Makefile" ")"`
	rm -r $(sather_home)/System/Platforms/linux/lwp
		
	install -m755 debian/bin-wrapper $(sather_home)/Bin
	
	ln -sf ../lib/sather/Bin/bin-wrapper $(sather_root)/usr/bin/sacomp
	
	dh_installmanpages -psather sabrowse.1 shortflat.1 tgrep.1
	dh_installchangelogs -psather Doc/Changes
	
	##### Installing: sather-lwp
	
	cp -a -P System/Platforms/linux/lwp $(lwp_home)
	rm $(lwp_home)/System/Platforms/linux/lwp/Makefile
	cp -a -P System/Common/Brahma/lib/lwp_linux.a $(lwp_home)
	cp -a -P System/Common/Brahma/lwp_linux/lwp_linux.h $(lwp_home)
	cp -a -P System/Common/Brahma/brahma.h $(lwp_home)
	cp -a -P System/pSather/lib $(lwp_home)
	cp -a -P System/pSather/include $(lwp_home)
	
	cp -a -P pLibrary $(lwp_home)
	
	rm -r `find $(lwp_root) "(" -name "*.config" -o -name "CVS" -o -name "Makefile" -o -name "PP" ")"`
	
	##### Installing: sather-browser
	
	for file in $(browser_files); do \
	  cp -a $$file $(browser_home)/$$file; \
	done
	
	ln -sf ../lib/sather/Bin/bin-wrapper $(browser_root)/usr/bin/sabrowse
	
	dh_installmanpages -psather-browser sacomp.1 shortflat.1 tgrep.1
	dh_installchangelogs -psather-browser
	
	##### Installing: sather-doc
	
	for file in $(info_files); do \
	    cp -a Emacs/$$file $(doc_info); \
	done
	
	cp -a Doc/Gui/Doc.fmk.ps $(doc_dir)/gui.ps
	
	dh_installchangelogs -psather-doc
	
	# Installing: sather-elisp
	
	cp -a $(el_files) $(el_dir)
	cp -a debian/50sather.el $(el_etc)
	cp -a Emacs/sather-mode.info $(el_info)
	
	dh_installchangelogs -psather-elisp Emacs/ChangeLog
	
	
	##### General finishing commands
	
	dh_installdocs
	dh_installemacsen
	
#	dh_installexamples
#	dh_installmenu
	
	dh_strip
	dh_compress
	dh_fixperms
	
	touch install-stamp

###################
	
# Build architecture-independent files here.
binary-indep: install-stamp
#	dh_testversion
	dh_testdir
	dh_testroot
	
#	dh_suidregister -i
	dh_installdeb -i
	dh_shlibdeps -i
	dh_gencontrol -i
	dh_makeshlibs -i
	dh_md5sums -i
	dh_builddeb -i


# Build architecture-dependent files here.
binary-arch: install-stamp
#	dh_testversion
	dh_testdir
	dh_testroot
	
#	dh_suidregister -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_makeshlibs -a
	dh_md5sums -a
	dh_builddeb -a


binary: binary-indep binary-arch

###################

clean: clean-internal
	$(MAKE) clean

clean-internal:
#	dh_testversion
	dh_testdir
	dh_testroot
	
	rm -f *-stamp
	
	dh_clean

# Meant for being called on any system (even non-Debian) without
# errors
clean-safe:
	if [ -x /usr/bin/dh_clean ] ; then                     \
	  if [ "$$UID" != "0" -a -x /usr/bin/fakeroot ] ; then \
	    fakeroot debian/rules clean-internal ;             \
	  else                                                 \
	    debian/rules clean-internal ;                      \
	  fi ;                                                 \
	fi

###################

test:
#	dh_testversion
	dh_testdir
	dh_testroot

source diff:                                                                  
	@echo >&2 'source and diff are obsolete - use dpkg-source -b'; false

.PHONY: build build-force install install-force \
        binary-indep binary-arch binary \
	clean test
        
############################################################################

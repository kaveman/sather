/*------------------------->  ANSI C - headerfile  <-------------------------*/
/* Copyright (C) 199x by International Computer Science Institute            */
/* This file is part of the GNU Sather library. It is free software; you may */
/* redistribute  and/or modify it under the terms of the GNU Library General */
/* Public  License (LGPL)  as published  by the  Free  Software  Foundation; */
/* either version 3 of the license, or (at your option) any later version.   */
/* This  library  is distributed  in the  hope that it will  be  useful, but */
/* WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY */
/* or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       */
/* The license text is also available from:  Free Software Foundation, Inc., */
/* 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     */
/*------------>  Please email comments to <bug-sather@gnu.org>  <------------*/

#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include "thread_queue.h"
#include "bundle_queue.h"
#include "fifo.h"
#include "lifo.h"
#include "mcs.h"
#include "fifo_lazy.h"
#include "lifo_lazy.h"
#include "mcs_lazy.h"
#include "mig.h"
#include "lff.h"
#include "crt.h"

#endif /* _SCHEDULER_H_ */

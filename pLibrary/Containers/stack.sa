------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1995 by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- Author: Benedict A. Gomes <gomes@icsi.berkeley.edu>

abstract class $STACK{T} < $NR_STACK{T}, $REENTRANT is --NR:
--NR: abstract class $NR_STACK{T} < $DISPENSER{T} is
   -- An abstract stack
   
   -- size: INT;
   -- current: T;
   -- remove: T;
   -- elt!: T;
   -- str: STR;
   -- copy: SAME
   -- has(e: T): BOOL;

   push(elt: T);
   -- Push elt to the top of the stack
   
   pop: T;
   -- Return and remove the topmost element
   
   top: T;
   -- Return the topmost element
   
   size: INT;
   -- Number of elements in the stack
   
   is_empty: BOOL;
   -- True if size = 0

   elt!: T;
   -- Return the elements in the order in which successive "pop"s
   -- would yield them without actually removing the elements
   
   copy: SAME; --NR: copy: SAME;
   -- Return a copy of the stack
   
end; -- abstract class $NR_STACK{T}
-------------------------------------------------------------------

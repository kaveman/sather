------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 199x by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

-- This lock uses a spinlock on machines that provide this
-- facilities. It is intended to guard small critical regions
-- in lock classes and not meant to be generally used.
class SPINLOCK is
   try:BOOL is builtin SPINLOCK_TRY; end;
   unlck is builtin SPINLOCK_UNLCK; end;
   lck is builtin SPINLOCK_LCK; end;

   -- Preconditions removed to remove dependance on Sather 1.1
   -- distributed extension (zones don't have near() expression)
   l_try:BOOL -- pre near(self) 
      is builtin SPINLOCK_L_TRY; end;
   l_unlck -- pre near(self) 
      is builtin SPINLOCK_L_UNLCK; end;
   l_lck -- pre near(self) 
      is builtin SPINLOCK_L_LCK; end;
   private init -- pre near(self) 
      is builtin SPINLOCK_INIT; end;

   create:SAME is 
      r::=new;
      r.init;
      return r;
   end;
end;


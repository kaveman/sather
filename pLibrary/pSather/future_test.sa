------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 199x by International Computer Science Institute            --
-- This file is part of the GNU Sather library. It is free software; you may --
-- redistribute  and/or modify it under the terms of the GNU Library General --
-- Public  License (LGPL)  as published  by the  Free  Software  Foundation; --
-- either version 3 of the license, or (at your option) any later version.   --
-- This  library  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/LGPL for more details.       --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

class TEST_FUTURE is
   include R_TEST;

   calc(m:$LOCK):INT is
      sync;
      test("sync",true,true);
      lock when m then
         return 10;
      end;
   end;

   main is
      class_name("FUTURE{INT}");
      f::=#FUTURE{INT};
      m::=#MUTEX;
      lock when f then
         test("f as mutex",true,true);
      else
         test("f as mutex",false,true);
      end;

      lock when f.has_value then
         test("f as has_value",true,false);
      else
         test("f as has_value",false,false);
      end;

      lock when f.has_thread then
         test("f as has_thread",true,false);
      else
         test("f as has_thread",false,false);
      end;

      lock when f.no_thread then
         test("f as no_thread",true,true);
      else
         test("f as no_thread",false,true);
      end;

      lock when f.no_value then
         test("f as no_value",true,true);
      else
         test("f as no_value",false,true);
      end;
      test("f is_used",f.is_used,false);
      test("f is_finished",f.is_finished,false);

      (* Had to comment out - this deadlocks on both SPARCs and ALPHAs
      lock when m then
         f :- calc(m)@1%clusters;

	 lock when f.has_value then
	    test("f as has_value 2",true,false);
	 else
	    test("f as has_value 2",false,false);
	 end;

	 lock when f.has_thread then
	    test("f as has_thread 2",true,true);
	 else
	    test("f as has_thread 2",false,true);
	 end;

	 lock when f.no_thread then
	    test("f as no_thread 2",true,false);
	 else
	    test("f as no_thread 2",false,false);
	 end;

	 lock when f.no_value then
	    test("f as no_value 2",true,true);
	 else
	    test("f as no_value 2",false,true);
	 end;
	 test("f is_used 2",f.is_used,true);
	 test("f is_finished 2",f.is_finished,false);
      end;
      SYS::defer;   -- Had to insert this to make work on ALPHA
      SYS::defer;   -- I guess, there is busy-waiting some place in the lock manager
      SYS::defer;   -- This breaks on a single CPU!
      SYS::defer;
      SYS::defer;
      test("reading value",f.get,10);

      
      lock when f.has_value then
	 test("f as has_value 3",true,true);
      else
	 test("f as has_value 3",false,true);
      end;

      lock when f.has_thread then
	 test("f as has_thread 3",true,false);
      else
	 test("f as has_thread 3",false,false);
      end;

      lock when f.no_thread then
	 test("f as no_thread 3",true,true);
      else
	 test("f as no_thread 3",false,true);
      end;

      lock when f.no_value then
	 test("f as no_value 3",true,false);
      else
	 test("f as no_value 3",false,false);
      end;
      test("f is_used 3",f.is_used,true);
      test("f is_finished 3",f.is_finished,true);

      *)
      finish;
   end;
    
end; -- class GATE_TEST

-------------------------------------------------------------------

export PATH_SEP    := /
export EXEC_SUFFIX :=
export RANLIB      := ranlib
export MV          := mv
export AR          := ar
export RM          := rm -f
export LN          := ln -sf
export CP          := cp
export CPP         := cpp -C -P
export CC          := gcc

export PLATFORMS   := linux
export DEFAULT_PLATFORM := linux

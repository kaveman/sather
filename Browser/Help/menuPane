General Notes
-------------
	It is possible to "tear-off" any menu by holding down the second
mouse button and dragging the menu away. The menu can be unposted by
clicking on the original menu button with the left mouse button

File
----
	Mainly added so that the quit button is in a standard location!

Quit Confirmation [on]
		 Turning this off allows you to quit without the 
		  confirming dialog box.
Save Configuration 
		Many aspects of your current configuration will be saved to
		file (~/.bsConfig.tcl), including the visibility of various 
		graph and feature elements.
Dump All State 		 Dump information to file - used to generate HTML.
			 See help.NewStuff

Quit		Exit the browser

Configuration
-------------
	These are various choices of window configuration etc. that you
can then preserve by saving your configuration
MultiWindow     Turning this on/off changes between the two major window
	        configurations, one with the graph window separate.
		You will be prompted to save the browser configuration
		file.
		For the new configuration to take effect the browser must
		be restarted.
Equal Split     Options to adjust the relative sizes of the top and bottom
		portions of the window
...
		   


Graph
-----
	The graph menu determines the nature of the graph that is displayed in
the graph window. 
Graph Node Types [check buttons]
concrete [on]	Display concrete classes
abstract [on]	Display abstract classes
modules  [on]	Display modules
                There is now the ability (courtesy Yon Uriarte) to 
	        hide/show classnames with a prefix of C_ or TEST_
  	        At all times the currently selected node will be displayed
Singleton Nodes  [on]  View or hide unconnected nodes (the selected node
			is always shown, even if it is a singleton)
Horizontal graph [on]  View the graph from left-to-right
Fixed Size Graph [on]  Indicates that the graph will be forced to fit on
		       one screen, possibly forcing the font to be unreadable
		       if the graph is very large. When it is off, the
		       graph (if large) will be allowed to take as much space
		       as it wants.
Graph update [on]      For (much) faster browsing, you may want to turn off 
		       graph updating.

Toggle Freeze [off]    Determines whether the currently selected class is
		       frozen or can change - same as the freeze button.

Save Current Layout	Save the current graph's layout to a file with the
			suffix .lay in the current directory.
Read Layout		Attempt to read a layout file with locations and
			a scale for the current set of nodes
Always Look For Layout[off] 
			When this is on, whenever a node is selected the
			browser looks for a layout file for the graph
			associated with that node (in the current directory) 
			and uses it if it is available under the name 
			<current node name>.lay

Dump Graph (PostScript)  Dump the current graph to a file as postscript

Features
--------
	The view menu determines which routines and classes will be visible.
You might find it handy to tear off this menu, as described above.
Viewing features [on]
	The first set of feature viewing menu items are self explanatory.
	Note that these menu items are not mutually exclusive. A feature
might well be private shared attribute. It will be displayed only if
*all* the relevant factors are turned on.

Text
----
Comment highlighting [on]:
	Comment highlighting (mostly courtesy DavidS) is neat, but
	slow. We tried to make it as fast as we could using tcl,
	but sadly had to decide to limit it to files of less than
	a threshold (gCommentThreshold in browser.tcl).
	If you still find it too slow, you may turn it off altogether
	or limit it to a different file size
Use Emacs [off]:
	Allows you to use an emacs buffer for editing. This must be 
	combined with the new emacs elisp which includes patches
	to server.el from Kevin.
	See help.emacs for more information

History
-------
View History
	Opens the history window, from which previous visits may be easily
	selected.
Close History
	Closes the history window. However "gHistoryLimit" entries are saved
Setting the history limit
	Determines how many history entries are saved when the history
	window is not visible. While the window is visible, no entries
	are deleted
History at startup
	Determines whether the history window will be visible at startup.
	Save the configuration (under the configuration menu) for this to
	take effect.


	



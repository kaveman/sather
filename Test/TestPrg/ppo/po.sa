------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 199x by International Computer Science Institute            --
-- This file is part of the GNU Sather package. It is free software; you may --
-- redistribute  and/or modify it under the terms of the  GNU General Public --
-- License (GPL)  as  published  by the  Free  Software  Foundation;  either --
-- version 3 of the license, or (at your option) any later version.          --
-- This  program  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/GPL for more details.        --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

immutable class V1{T} < $OB is
   include AVAL{T};
   const asize:=100;
   attr a:INT;
   attr b:FLT;
   attr c:FLTD;
   attr d:BOOL;
   attr e:CHAR;
   attr f:STR;
   attr g:FSTR;
   attr t:$OB;
end;

class R1{T} < $OB is
   include AREF{T};
   attr a:INT;
   attr b:FLT;
   attr c:FLTD;
   attr d:BOOL;
   attr e:CHAR;
   attr f:STR;
   attr g:FSTR;
   attr t:$OB;
   create:SAME is return new(200); end;
end;

class R < $OB is
   create:SAME is return new; end;

   attr va:V1{INT};
   attr vb:V1{FLT};
   attr vc:V1{FLTD};
   attr vd:V1{BOOL};
   attr ve:V1{CHAR};
   attr vf:V1{STR};
   attr vg:V1{FSTR};
   attr vh:V1{$OB};

   attr ra:R1{INT};
   attr rb:R1{FLT};
   attr rc:R1{FLTD};
   attr rd:R1{BOOL};
   attr re:R1{CHAR};
   attr rf:R1{STR};
   attr rg:R1{FSTR};
   attr rh:R1{$OB};

   attr v1,v2:$OB;
end;

immutable class VV is attr a,b:INT; end;

class MAIN is
   shared cl:INT;
   next_cl is
      i::=here;
      v:VV;
      v:=v.a(here).b(here);
      if here=cl then
         PO::PT;
      else
         next_cl@(here+1)%clusters;
      end;
   end;
   main(argv:ARRAY{STR}) is
      cl:=#(argv[1]);
      argv[1]:="-"; -- make sure that we get the same result in the 
      		   -- printout regardless of the argument
      PO::print_gdb(0)@cl;
      PO::print_id(0)@cl;
      PO::print_pointer(0)@cl;
      PO::print_type(1)@cl;
      PO::print_real(1)@cl;
      PO::print_depth(5)@cl;

      next_cl@(cl+1)%clusters;

      r::=#R;
      r.ra:=#;
      r.rb:=#;
      r.rc:=#;
      r.rd:=#;
      r.re:=#;
      r.rf:=#;
      r.rg:=#;
      r.rh:=#;
      r.va:=r.va.aset(2,20).aset(3,30).a(40);
      r.vb:=r.vb.aset(2,2.2).aset(3,3.3).b(4.4).t(r.va);
      r.vc:=r.vc.aset(2,2.22.fltd).aset(3,3.33.fltd).c(4.44.fltd).t(r.rb);
      r.vd:=r.vd.aset(2,true).aset(3,true).d(true);
      r.ve:=r.ve.aset(2,'a').aset(3,'b').e('c');
      r.vf:=r.vf.aset(2,"AAAA").aset(3,"BBBB").f("CCCC");
      r.vg:=r.vg.aset(2,"EEEE".fstr).aset(3,"RRRR".fstr).g("TTTT".fstr);
      r.vh:=r.vh.aset(2,"STRING").aset(3,123).aset(4,r.rc).aset(5,r.vc);
      r.ra[4]:=120;r.ra[5]:=130;r.ra.a:=140;
      r.rb[4]:=1.2;r.rb[5]:=1.3;r.rb.b:=1.4;
      r.rc[4]:=1.22.fltd;r.rc[5]:=1.33.fltd;r.rc.c:=1.44.fltd;
      r.rd[4]:=true;r.rd[5]:=true;r.rd.d:=true;
      r.re[4]:='e';r.re[5]:='f';r.re.e:='g';
      r.rf[4]:="AaAa";r.rf[5]:="BbBb";r.rf.f:="CcCc";
      r.rg[4]:="EeEe".fstr;r.rg[5]:="RrRr".fstr;r.rg.g:="TtTt".fstr;
      r.rh[4]:="string";r.rh[5]:=1234;r.rh[6]:=true;r.rh[7]:=r.vd;r.rh[8]:=r.rd;

      r.v1:=r.va;
      r.v2:=r.ra;

      o:$OB:=r;
      PO::PO(o)@cl;
      PO::PT@cl;
      PO::PF@cl;
   end;
end;




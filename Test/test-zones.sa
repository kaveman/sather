------------------------->  GNU Sather - sourcefile  <-------------------------
-- Copyright (C) 1994 by International Computer Science Institute            --
-- This file is part of the GNU Sather package. It is free software; you may --
-- redistribute  and/or modify it under the terms of the  GNU General Public --
-- License (GPL)  as  published  by the  Free  Software  Foundation;  either --
-- version 3 of the license, or (at your option) any later version.          --
-- This  program  is distributed  in the  hope that it will  be  useful, but --
-- WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE. See Doc/GPL for more details.        --
-- The license text is also available from:  Free Software Foundation, Inc., --
-- 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                     --
-------------->  Please email comments to <bug-sather@gnu.org>  <--------------

class TEST_ZONES is

    include TEST;
 
    testzones is

	class_name("Zones");

	test("global at startup",(here=global).str,"true");
	test("zone reflexive equality",(global=global).str,"true");
	z::=#ZONE;

	test("zone inequality",(z=global).str,"false");
	test("zone inequality2",z<global or z>global,"true");
	test("zone inequality3",z<global and z>global,"false");

        ob::=#FLIST{INT}(10);
        ob2::=#FLIST{INT}(10) @ z;

	test("still global after at expr",(here=global).str,"true");

	test("allocation",(where(ob)=here).str,"true");
	test("allocation2",(where(ob2)=z).str,"true");

        helper(z) @ z;

	test("still global after at expr2",(here=global).str,"true");

        r::=helper2(z) @ z;

	test("returned value",r,"xyz");

	test("still global after at expr3",(here=global).str,"true");

	test("capacity",(global.capacity>0).str,"true");

	if global.divisions>0 then
           c::=0;
	   loop i::=global.divisions.times!;
	      test("div "+i+"a",global.division(i).within(global).str,"true");
	      test("div "+i+"b",global.within(global.division(i)).str,"false");
	      test("cap "+i,(global.division(i).capacity>0).str,"true");
	      c:=c+global.division(i).capacity;
	   end;
	   test("capacity match",(global.capacity=c).str,"true");
	end;

	zlist::=#ARRAY{ZONE}(100);
	znext::=global;
	zlast:ZONE;
	loop i::=zlist.ind!;
	   zlast:=znext;
	   znext:=#ZONE @ zlast;
	   zlist[i]:=znext;
           test("parenting "+i,znext.within(zlast).str,"true");
           test("ancestry "+i,znext.within(global).str,"true");
	end;

        -- Force some garbage collection or at least lots of allocation
	loop i::=zlist.ind!;
	   loop 1000.times!;
	      dummy::=#ARRAY{INT}(RND::int(0,10)) @ zlist[i];
	      -- Too much output, just use asserts
	      assert(where(dummy)=zlist[i]);
	      assert(where(dummy).within(global));
	      assert(where(dummy).within(where(zlist[i])));
	   end;
	end;

        finish;

    end;

    helper(z:ZONE) is
       test("inside at",here=z,"true");
    end;

    helper2(z:ZONE):STR is
       test("inside at",here=z,"true");
       return "xyz";
    end;

    main is
       testzones;
       #ERR+"\nTEST DONE\n";
    end;

end;
